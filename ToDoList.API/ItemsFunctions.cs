using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ToDoList.BLL;

namespace ToDoList.API
{
    public class ItemsFunctions
    {
        private ToDoItemManager itemManager;

        public ItemsFunctions(ToDoListDbContext context)
        {
            itemManager = new ToDoItemManager(context);
        }

        [FunctionName("AddItem")]
        public ActionResult<ToDoItem> AddItem(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "item")] HttpRequest req,
            ILogger log)
        {
            AddItemDTO addItemData = null;
            try
            {
                string requesteBody = new StreamReader(req.Body).ReadToEnd();
                addItemData = JsonConvert.DeserializeObject<AddItemDTO>(requesteBody);
                var addedItem = itemManager.AddItem(addItemData);
                return addedItem;
            }
            catch (Exception e)
            {
                var result = new ObjectResult(e);
                result.StatusCode = StatusCodes.Status500InternalServerError;
                return result;
            }
        }
    }
}