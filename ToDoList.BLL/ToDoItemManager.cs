﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDoList.BLL
{
    public class ToDoItemManager
    {
        private readonly ToDoListDbContext _context;

        public ToDoItemManager(ToDoListDbContext context)
        {
            _context = context;
        }

        public ToDoItem AddItem(AddItemDTO addItemDto)
        {
            ToDoItem newItemToAdd = new ToDoItem()
            {
                CreatedAt = DateTime.Now,
                Description = addItemDto.Description,
                State = ItemStates.New
            };

            _context.Add(newItemToAdd);
            _context.SaveChanges();
            return newItemToAdd;
        }
    }
}